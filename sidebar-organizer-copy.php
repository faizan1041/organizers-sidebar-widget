<?php
/*
Plugin Name: Organizers Sidebar Widget
Plugin URI: http://faizan-ali.com/
Description: A widget which displays a post from your neighbour on the site.
Author: Faizan Ali
Version: 1.0
Author URI: http://faizan-ali.com/
*/


/**
 * Adds Orgn_Sidebar_Widget widget.
 */







function register_Orgn_Sidebar_Widget() {
    register_widget( 'Orgn_Sidebar_Widget');
}

add_action( 'widgets_init', 'register_Orgn_Sidebar_Widget' );






class Orgn_Sidebar_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'Orgn_Sidebar_Widget', // Base ID
			__('Organizers Sidebar Widget', 'text_domain'), // Name
			array( 'description' => __( 'An Organizers Sidebar Widget', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {





         $current_user = wp_get_current_user();

        $user_zip = $this->orgsb_getuserszip($current_user->ID);


        if(count($user_zip)!=0) {

            $user_zip =  $user_zip[0]->value;
              }

        $user_neigb = $this->orgsb_getusersneigb($current_user->ID);



        if(count($user_neigb)!=0) {

        $user_neigb =  $user_neigb[0]->value;



        }



        $users_tax = array($user_zip,$user_neigb);

         $args = array(
                'post_type' => $instance['post_type'],
                'posts_per_page' =>  $instance['number_of_posts'],
                'tax_query' => array(
                    'relation' => 'AND',
                    array(
                        'taxonomy' => 'post_tag',
                        'field'    => 'slug',
                        'terms'    => $users_tax,

                    ),

                ),

            /*    'order'   => 'ASC',
                'orderby' => 'meta_value_num',*/
            );






        $orgsbqry = new WP_Query($args);



if ( $orgsbqry->have_posts() ) {

    echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo '<h2 class="widgettitle"><span>
' . $instance['title'] . '</h2></span>';
		}

        else {
         echo '<br/>';
        }

    while ( $orgsbqry->have_posts() ) {

         $orgsbqry->the_post();

       if(has_term( $user_zip, 'post_tag' ) && has_term( $user_neigb, 'post_tag' )){


          echo '<div><h5><a href="'. get_permalink() .'">'. get_the_title() . '</a></h5>';
           echo '<a href="'. get_permalink() .'">'. get_the_post_thumbnail() . '</a><br/>';
            echo get_the_excerpt( ) . '</div>';

     }


      else if(has_term( $user_zip, 'post_tag' ) || has_term( $user_neigb, 'post_tag' )){

          echo '<div><h5><a href="'. get_permalink() .'">'. get_the_title() . '</a></h5>';
           echo '<a href="'. get_permalink() .'">'. get_the_post_thumbnail() . '</a><br/>';
            echo get_the_excerpt( ) . '</div>';


      }

        } // end while

    echo $args['after_widget'];


} //endif

         else if ( !$orgsbqry->have_posts() ){
              echo $args['before_widget'];
               if ( ! empty( $instance['title'] ) ) {
			echo '<h2 class="widgettitle"><span>
' . $instance['title'] . '</h2></span>';
		}

        else {
         echo '<br/>';
        }






               echo '<div><h5><a href="'. $instance['default_post_link'] .'">'.  $instance['default_post_title'] . '</a></h5>';
           echo '<a href="'. $instance['default_post_link'] .'"><img src="' . $instance['default_post_image_url'] . '" width="250" /></a>' . '<br/>';
            echo '<p>' . $instance['default_post_excerpt'] . '</p></div>';



       echo $args['after_widget'];
         }


       wp_reset_postdata();




	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'text_domain' );
		}
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php

        if ( isset( $instance[ 'number_of_posts' ] ) ) {
			$number_of_posts = $instance[ 'number_of_posts' ];
		}
		else {
			$number_of_posts = __( '1', 'text_domain' );
		}
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'number_of_posts' ); ?>"><?php _e( 'Number of Posts:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'number_of_posts' ); ?>" name="<?php echo $this->get_field_name( 'number_of_posts' ); ?>" type="number" value="<?php echo esc_attr( $number_of_posts ); ?>">
		</p>
		<?php



         if ( isset( $instance[ 'post_type' ] ) ) {
			$post_type = $instance[ 'post_type' ];
		}
		else {
			$post_type = __( '', 'text_domain' );
		}
		?>

		<p>
		<label for="<?php echo $this->get_field_id( 'post_type' ); ?>"><?php _e( 'Post Type:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'post_type' ); ?>" name="<?php echo $this->get_field_name( 'post_type' ); ?>" type="text" value="<?php echo esc_attr( $post_type ); ?>">
		</p>
		<?php











         if ( isset( $instance[ 'default_post_title' ] ) ) {
			$default_post_title = $instance[ 'default_post_title' ];
		}
		else {
			$default_post_title = __( '', 'text_domain' );
		}
		?>
        <p>Default Post to show when there's no matching post for the organizer</p>

		<p>
		<label for="<?php echo $this->get_field_id( 'default_post_title' ); ?>"><?php _e( 'Post Title: ' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'default_post_title' ); ?>" name="<?php echo $this->get_field_name( 'default_post_title' ); ?>" type="text" value="<?php echo esc_attr( $default_post_title ); ?>">
		</p>
		<?php


          if ( isset( $instance[ 'default_post_image_url' ] ) ) {
			$default_post_image_url = $instance[ 'default_post_image_url' ];
		}
		else {
			$default_post_image_url = __( '', 'text_domain' );
		}
		?>

		<p>
		<label for="<?php echo $this->get_field_id( 'default_post_image_url' ); ?>"><?php _e( 'Image URL:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'default_post_image_url' ); ?>" name="<?php echo $this->get_field_name( 'default_post_image_url' ); ?>" type="text" value="<?php echo esc_attr( $default_post_image_url ); ?>">
		</p>
		<?php






		if ( isset( $instance[ 'default_post_excerpt' ] ) ) {
			$default_post_excerpt = $instance[ 'default_post_excerpt' ];
		}
		else {
			$default_post_excerpt = __( '', 'text_domain' );
		}
		?>

		<p>
		<label for="<?php echo $this->get_field_id( 'default_post_excerpt' ); ?>"><?php _e( 'Post Excerpt:' ); ?></label>
			<textarea class="widefat" id="<?php echo $this->get_field_id( 'default_post_excerpt' ); ?>" name="<?php echo $this->get_field_name( 'default_post_excerpt' ); ?>"  ><?php echo esc_attr( $default_post_excerpt ); ?></textarea>
		</p>
		<?php



		 if ( isset( $instance[ 'default_post_link' ] ) ) {
			$default_post_link = $instance[ 'default_post_link' ];
		}
		else {
			$default_post_link = __( '', 'text_domain' );
		}
		?>

		<p>
		<label for="<?php echo $this->get_field_id( 'default_post_link' ); ?>"><?php _e( 'Post Link:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'default_post_link' ); ?>" name="<?php echo $this->get_field_name( 'default_post_link' ); ?>" type="text" value="<?php echo esc_attr( $default_post_link ); ?>">
		</p>
		<?php





	}





	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

        $instance['number_of_posts'] = ( ! empty( $new_instance['number_of_posts'] ) ) ? strip_tags( $new_instance['number_of_posts'] ) : '';

         $instance['post_type'] = ( ! empty( $new_instance['post_type'] ) ) ? strip_tags( $new_instance['post_type'] ) : '';


        $instance['default_post_title'] = ( ! empty( $new_instance['default_post_title'] ) ) ? strip_tags( $new_instance['default_post_title'] ) : '';

        $instance['default_post_image_url'] = ( ! empty( $new_instance['default_post_image_url'] ) ) ? strip_tags( $new_instance['default_post_image_url'] ) : '';

		$instance['default_post_excerpt'] = ( ! empty( $new_instance['default_post_excerpt'] ) ) ? strip_tags( $new_instance['default_post_excerpt'] ) : '';

		$instance['default_post_link'] = ( ! empty( $new_instance['default_post_link'] ) ) ? strip_tags( $new_instance['default_post_link'] ) : '';




		return $instance;
	}


    public function orgsb_getuserszip($user_id) {

     global $wpdb;

        $sql = "SELECT " . $wpdb->prefix . "bp_xprofile_data.user_id, " . $wpdb->prefix . "bp_xprofile_fields.name, " . $wpdb->prefix . "bp_xprofile_data.value FROM " . $wpdb->prefix . "bp_xprofile_fields LEFT JOIN " . $wpdb->prefix . "bp_xprofile_data ON " . $wpdb->prefix . "bp_xprofile_fields.id = " . $wpdb->prefix . "bp_xprofile_data.field_id WHERE " . $wpdb->prefix . "bp_xprofile_fields.name = 'Zip' AND " . $wpdb->prefix . "bp_xprofile_data.user_id = " . $user_id ;

        $qry = $wpdb->get_results($sql);


    return $qry;

    }


     public function orgsb_getusersneigb($user_id) {

     global $wpdb;

        $sql = "SELECT " . $wpdb->prefix . "bp_xprofile_data.user_id, " . $wpdb->prefix . "bp_xprofile_fields.name, " . $wpdb->prefix . "bp_xprofile_data.value FROM " . $wpdb->prefix . "bp_xprofile_fields LEFT JOIN " . $wpdb->prefix . "bp_xprofile_data ON " . $wpdb->prefix . "bp_xprofile_fields.id = " . $wpdb->prefix . "bp_xprofile_data.field_id WHERE " . $wpdb->prefix . "bp_xprofile_fields.name = 'Neighborhood' AND " . $wpdb->prefix . "bp_xprofile_data.user_id = " . $user_id ;

        $qry = $wpdb->get_results($sql);


    return $qry;

    }


    public function orgsb_get_the_excerpt($post_id) {
          global $post;
          $save_post = $post;
          $post = get_post($post_id);
          $output = get_the_excerpt();
          $post = $save_post;
          return $output;
        }



} // class






